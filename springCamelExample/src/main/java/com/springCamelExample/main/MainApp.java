package com.springCamelExample.main;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.json.JSONObject;
import org.springframework.http.converter.json.JsonbHttpMessageConverter;

import com.springCamelExample.routes.DirectRoute;


public class MainApp {

	public static void main(String[] args) {
		
		CamelContext ctx = new DefaultCamelContext();
		ProducerTemplate prod = ctx.createProducerTemplate();
		ConsumerTemplate consu = ctx.createConsumerTemplate();
		
		try {	
			System.out.println("App Started");
			
			ctx.start();
			System.out.println("ctx start");
			
			
			ctx.addRoutes(new DirectRoute());
			System.out.println("Adding routes");
			
			
			JSONObject enviado = new JSONObject();
			enviado.put("msj", "Hello Aliens! This body was sent from the producer");
			enviado.put("token", false);
			System.out.println("JSon 'enviado' armado");
			System.out.println(enviado.toString());
			
			
			prod.sendBody("direct:start", enviado);
			System.out.println("Enviado");
			JSONObject recibido = consu.receiveBody("seda:true",JSONObject.class);
//			System.out.println("recibido en true");
//			JSONObject recibi2 = consu.receiveBody("seda:false", JSONObject.class);
//			System.out.println("recibido en false");
//			Thread.sleep(1000);
			
			
			if(recibido != null) {
			System.out.println(recibido.get("msj"));			
			System.out.println(recibido.get("error"));
//			}else if(recibi2 !=null){
//				System.out.println(recibi2.get("msj"));			
//				System.out.println(recibi2.get("error"));
//				
			}else {
				System.out.println("msj vacio");
			}

		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}//Cierre MAIN
