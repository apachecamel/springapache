package com.springCamelExample.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.spi.Synchronization;
import org.json.JSONObject;

public class MyProcessor implements Processor{

	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Procesador IN");
		Message s = exchange.getIn();
		JSONObject j = (JSONObject) s.getBody();
		JSONObject j2 = new JSONObject();
		j2.put("msj",j.get("msj")+" Esto es lo del proceso");
		
		if(!j.getBoolean("token")) {
			j2.put("token", !j.getBoolean("token"));
			j2.put("msj", j2.get("msj")+ " el mensaje fue validado");
			j2.put("error",0);
		}else {
			j2 = null;
		}
		s.setBody(j2, JSONObject.class);
		exchange.setOut(s);  
		System.out.println("Procesador OUT");
		
	}

}
