package com.springCamelExample.routes;

//import org.apache.camel.CamelContext;
//import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;

import com.springCamelExample.processor.MyProcessor;

public class DirectRoute extends RouteBuilder {


	@Override
	public void configure() {
//			from("direct:start").
//			process(new MyProcessor()).
//			choice().
//			when(outBody().contains(true)).
//			to("seda:end").
//			otherwise().to("seda:end2");
		
		from("direct:start").process(new MyProcessor()).
		choice().
		when(body().isNotNull()).to("seda:true")
		.otherwise().log("No message");
	
	}
}
